swag-init:
	swag init -g api/api.go -o api/docs
	
run: 
	go run cmd/main.go

install:
	swag init -g api/api.go -o api/docs
	go mod download
	go mod vendor
	go run cmd/main.go
	
up:
	migrate -path ./migrations -database 'postgres://postgres:admin@localhost:5432/bootcamp?sslmode=disable' up
down:
	migrate -path ./migrations -database 'postgres://postgres:admin@localhost:5432/bootcamp?sslmode=disable' down