package postgres

import (
	"bootcamp/article/models"
	"bootcamp/article/storage"
	"database/sql"
	"github.com/jmoiron/sqlx"
	"time"
)

type articleRepo struct {
	db *sqlx.DB
}

func NewArticleRepo(db *sqlx.DB) storage.ArticleRepoI {
	return articleRepo{
		db: db,
	}
}

func (r articleRepo) Create(entity models.ArticleCreateModel) (err error) {
	insertQuery := `INSERT INTO article (
		title,
		body,
		author_id
	) VALUES (
		$1,
		$2,
		$3
	)`

	_, err = r.db.Exec(insertQuery,
		entity.Title,
		entity.Body,
		entity.AuthorID,
	)

	return err
}

func (r articleRepo) GetList(query models.Query) (resp []models.ArticleListItem, err error) {
	var rows *sql.Rows
	if len(query.Search) > 0 {
		rows, err = r.db.Query(
			`SELECT
			ar.id, ar.title, ar.body, ar.created_at, ar.updated_at,
			au.id, au.firstname, au.lastname, au.created_at, au.updated_at
			FROM article AS ar JOIN author AS au ON ar.author_id = au.id
			WHERE title ILIKE '%' || $3 || '%'
			ORDER BY ar.id
			OFFSET $1 LIMIT $2
			`,
			query.Offset,
			query.Limit,
			query.Search,
		)
	} else {
		rows, err = r.db.Query(
			`SELECT
			ar.id, ar.title, ar.body, ar.created_at, ar.updated_at,
			au.id, au.firstname, au.lastname, au.created_at, au.updated_at
			FROM article AS ar JOIN author AS au ON ar.author_id = au.id
			ORDER BY ar.id
			OFFSET $1 LIMIT $2
			`,
			query.Offset,
			query.Limit,
		)
	}

	if err != nil {
		return resp, err
	}

	defer rows.Close()
	for rows.Next() {
		var a models.ArticleListItem
		err = rows.Scan(
			&a.ID, &a.Title, &a.Body, &a.CreatedAt, &a.UpdatedAt,
			&a.Author.ID, &a.Author.Firstname, &a.Author.Lastname, &a.Author.CreatedAt, &a.Author.UpdatedAt,
		)
		resp = append(resp, a)
		if err != nil {
			return resp, err
		}
	}

	return resp, err
}

func (r articleRepo) GetByID(ID int) (resp models.Article, err error) {
	rows, err := r.db.Query(
		"SELECT id, title, body, author_id, created_at, updated_at FROM article WHERE id=$1",
		ID,
	)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var a models.Article
		err = rows.Scan(&a.ID, &a.Title, &a.Body, &a.AuthorID, &a.CreatedAt, &a.UpdatedAt)

		if err != nil {
			return resp, err
		}
		resp = a
	}

	return resp, err
}

func (r articleRepo) Update(entity models.ArticleUpdateModel) (err error) {
	rows, err := r.db.Query(
		"UPDATE article SET title = $1, body = $2, updated_at = $3 WHERE id = $4",
		entity.Title,
		entity.Body,
		time.Now(),
		entity.ID,
	)

	defer rows.Close()

	if err != nil {
		return err
	}

	return err
}

func (r articleRepo) Delete(ID int) (err error) {
	rows, err := r.db.Query(
		"DELETE FROM article WHERE id = $1",
		ID,
	)
	defer rows.Close()

	if err != nil {
		return err
	}

	return err
}
