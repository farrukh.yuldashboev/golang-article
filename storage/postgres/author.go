package postgres

import (
	"bootcamp/article/models"
	"bootcamp/article/storage"
	"database/sql"
	"github.com/jmoiron/sqlx"
	"time"
)

type authorRepo struct {
	db *sqlx.DB
}

func NewAuthorRepo(db *sqlx.DB) storage.AuthorRepoI {
	return authorRepo{
		db: db,
	}
}

func (r authorRepo) Create(entity models.Person) (err error) {
	insertQuery := `INSERT INTO author (
		firstname,
		lastname
	) VALUES (
		$1,
		$2
	)`

	_, err = r.db.Exec(insertQuery,
		entity.Firstname,
		entity.Lastname,
	)

	return err
}

func (r authorRepo) GetList(query models.Query) (resp []models.PersonListModel, err error) {
	var rows *sql.Rows
	if len(query.Search) > 0 {
		rows, err = r.db.Query(
			`SELECT
			id, firstname, lastname, created_at, updated_at
			FROM author
			WHERE firstname ILIKE '%' || $3 || '%' OR lastname ILIKE '%' || $3 || '%'
			ORDER BY id
			OFFSET $1 LIMIT $2
			`,
			query.Offset,
			query.Limit,
			query.Search,
		)
	} else {
		rows, err = r.db.Query(
			`SELECT
			id, firstname, lastname, created_at, updated_at
			FROM author
			ORDER BY id
			OFFSET $1 LIMIT $2
			`,
			query.Offset,
			query.Limit,
		)
	}

	if err != nil {
		return resp, err
	}

	defer rows.Close()
	for rows.Next() {
		var a models.PersonListModel
		err = rows.Scan(
			&a.ID, &a.Firstname, &a.Lastname, &a.CreatedAt, &a.UpdatedAt,
		)
		resp = append(resp, a)
		if err != nil {
			return resp, err
		}
	}

	return resp, err
}

func (r authorRepo) GetByID(ID int) (resp models.PersonListModel, err error) {
	rows, err := r.db.Query(
		"SELECT id, firstname, lastname, created_at, updated_at FROM author WHERE id=$1",
		ID,
	)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var a models.PersonListModel
		err = rows.Scan(&a.ID, &a.Firstname, &a.Lastname, &a.CreatedAt, &a.UpdatedAt)

		if err != nil {
			return resp, err
		}
		resp = a
	}

	return resp, err
}

func (r authorRepo) Update(entity models.PersonUpdateModel) (err error) {
	rows, err := r.db.Query(
		"UPDATE author SET firstname = $1, lastname = $2, updated_at = $3 WHERE id = $4",
		entity.Firstname,
		entity.Lastname,
		time.Now(),
		entity.ID,
	)

	defer rows.Close()

	if err != nil {
		return err
	}

	return err
}

func (r authorRepo) Delete(ID int) (err error) {
	rows, err := r.db.Query(
		"DELETE FROM author WHERE id = $1",
		ID,
	)
	defer rows.Close()

	if err != nil {
		return err
	}

	return err
}
