package api

import (
	"bootcamp/article/api/docs"
	"bootcamp/article/api/handlers"
	"bootcamp/article/config"
	"github.com/gin-gonic/gin"
	ginSwagger "github.com/swaggo/gin-swagger"
	"github.com/swaggo/gin-swagger/swaggerFiles"
)

// @description Added Search functionality to GetByIDHandler.
// @termsOfService https://udevs.io
func SetUpApi(r *gin.Engine, h handlers.Handler, cfg config.Config) {

	docs.SwaggerInfo.Title = cfg.App
	docs.SwaggerInfo.Version = cfg.Version
	docs.SwaggerInfo.Host = cfg.ServiceHost + cfg.HTTPPort
	docs.SwaggerInfo.Schemes = []string{"http", "https"}

	r.POST("/articles", h.CreateHandler)
	r.GET("/articles", h.GetArticleList)
	r.GET("/articles/:id", h.GetByIDHandler)
	r.PATCH("/articles", h.UpdateHandler)
	r.DELETE("/articles/:id", h.DeleteHandler)

	r.POST("/authors", h.CreateHandlerAuthor)
	r.GET("/authors", h.GetAuthorList)
	r.GET("/authors/:id", h.GetByIDHandlerAuthor)
	r.PATCH("/authors", h.UpdateHandlerAuthor)
	r.DELETE("/authors/:id", h.DeleteHandlerAuthor)

	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
}
