package handlers

import (
	"bootcamp/article/models"
	"fmt"
	"github.com/gin-gonic/gin"
	"strconv"
)

// CreateHandler godoc
// @Router /articles [POST]
// @Summary Create an article
// @Description it creates an article based on input data
// @ID create-handeler
// @Tags CRUD Article
// @Accept  json
// @Produce  json
// @Param data body models.ArticleCreateModel true "Article data"
// @Success 200  {object} models.SuccessResponse
// @Failure 400,404 {object} models.ErrorResponse
// @Failure default {object} models.DefaultError
func (h *Handler) CreateHandler(c *gin.Context) {
	var article models.ArticleCreateModel
	err := c.BindJSON(&article)
	if err != nil {
		c.JSON(400, models.ErrorResponse{
			Code:    400,
			Message: err.Error(),
		})
		return
	}

	err = h.strg.Article().Create(article)
	if err != nil {
		c.JSON(400, models.DefaultError{
			Message: err.Error(),
		})
		return
	}

	c.JSON(200, models.SuccessResponse{
		Message: "Ok",
	})
}

// GetArticleList godoc
// @Router /articles [GET]
// @ID get-article-list
// @Summary List articles
// @Description get all articles
// @Param offset query int false "offset"
// @Param limit query int false "limit"
// @Param search query string false "search string"
// @Tags CRUD Article
// @Accept  json
// @Produce  json
// @Success 200 {array} models.ArticleListItem
// @Failure default {object} models.DefaultError
func (h *Handler) GetArticleList(c *gin.Context) {
	offset, err := h.getOffsetParam(c)
	if err != nil {
		c.JSON(400, models.DefaultError{
			Message: err.Error(),
		})
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		c.JSON(400, models.DefaultError{
			Message: err.Error(),
		})
	}

	resp, err := h.strg.Article().GetList(models.Query{Offset: offset, Limit: limit, Search: c.Query("search")})

	if err != nil {
		c.JSON(400, models.DefaultError{
			Message: err.Error(),
		})
	}

	c.JSON(200, resp)
}

// GetByIDHandler godoc
// @ID get-by-id-handler
// @Router /articles/{id} [GET]
// @Summary Get article by ID
// @Description it gets an article by ID
// @Tags CRUD Article
// @Accept  json
// @Produce  json
// @Param id path int true "Article ID"
// @Success 200 {object} models.Article
// @Header 200 {string} Token "qwerty"
// @Failure default {object} models.DefaultError
func (h *Handler) GetByIDHandler(c *gin.Context) {

	idStr := c.Param("id")
	idNum, err := strconv.ParseUint(idStr, 10, 64)

	if err != nil {
		c.JSON(400, models.DefaultError{
			Message: err.Error(),
		})
		return
	}

	resp, err := h.strg.Article().GetByID(int(idNum))

	if err != nil {
		c.JSON(400, models.DefaultError{
			Message: err.Error(),
		})
		return
	}

	c.JSON(200, gin.H{
		"res": resp,
	})
}

// UpdateHandler godoc
// @Router /articles [PATCH]
// @Summary Update article
// @Description it updates data from body
// @ID update-handler
// @Tags CRUD Article
// @Accept  json
// @Produce  json
// @Param data body models.ArticleUpdateModel true "Article data"
// @Success 200  {object} models.SuccessResponse
// @Failure 400 {object} models.ErrorResponse
// @Failure default {object} models.DefaultError
func (h *Handler) UpdateHandler(c *gin.Context) {
	var article models.ArticleUpdateModel
	err := c.BindJSON(&article)
	if err != nil {
		c.JSON(400, models.ErrorResponse{
			Code:    400,
			Message: err.Error(),
		})
		return
	}

	err = h.strg.Article().Update(article)
	if err != nil {
		fmt.Println("could not update article")
		c.JSON(400, models.ErrorResponse{
			Code:    400,
			Message: "could not update article",
		})
		return
	}

	c.JSON(200, gin.H{
		"res": "updated",
	})

}

// DeleteHandler godoc
// @Router /articles/{id} [DELETE]
// @Summary Delete Article by ID
// @Description it deletes single article by ID
// @Tags CRUD Article
// @Accept  json
// @Produce  json
// @Param id path int true "Article ID"
// @Success 200 {object} models.SuccessResponse
// @Header 200 {string} Token "qwerty"
// @Failure 400 {object} models.ErrorResponse
// @Failure default {object} models.DefaultError
func (h *Handler) DeleteHandler(c *gin.Context) {
	idStr := c.Param("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		c.JSON(400, models.ErrorResponse{
			Code:    400,
			Message: err.Error(),
		})
		return
	}

	err = h.strg.Article().Delete(id)
	if err != nil {
		c.JSON(400, models.ErrorResponse{
			Code:    400,
			Message: err.Error(),
		})
		return
	}

	c.JSON(200, models.SuccessResponse{
		Message: "deleted",
	})
}
